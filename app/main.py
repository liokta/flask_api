from flask import Flask
from flask_admin import Admin
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config

app = Flask(__name__)

app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

admin = Admin(app, name='Panel', template_mode='bootstrap3')

mail = Mail(app)