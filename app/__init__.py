from flask_admin.contrib.sqla import ModelView
from app.main import *
from app import models, celery_app

from app.services import *

admin.add_view(ModelView(models.Mail, db.session))
admin.add_view(ModelView(models.Recipient, db.session))

