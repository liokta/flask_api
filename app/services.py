import datetime
from app import app, celery_app, mail
from app.models import Mail, Recipient
from config import Config

from flask_mail import Message

celery = celery_app.make_celery(app)

@celery.task()
def send_mail():
    now = datetime.datetime.now()
    date_time = now.replace(second=0, microsecond=0)
    mails = Mail.query.filter(Mail.timestamp == date_time).all()
    recipients = Recipient.query.all()

    if recipients:
        email_recipients = [r.email for r in recipients]
        if mails:
            for m in mails:
                msg = Message(body=m.email_content, subject=m.email_subject, recipients=email_recipients)
                try:
                    mail.send(msg)
                except Exception as e:
                    print(e)
    return None