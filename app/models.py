from app import db
from sqlalchemy.types import TIMESTAMP

class Mail(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, nullable=False,)
    email_subject = db.Column(db.String(256), nullable=False,)
    email_content = db.Column(db.Text, nullable=False,)
    timestamp = db.Column(TIMESTAMP, nullable=False,)

    def __repr__(self):
        return '<Mail {}>'.format(self.event_id)



class Recipient(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(256), nullable=False, )

    def __repr__(self):
        return '<Recipient {}>'.format(self.email)

