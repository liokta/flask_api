import datetime

from flask import request, jsonify
from flask_restplus import Api, Resource, reqparse
from werkzeug.exceptions import BadRequest
from app import app, db
from app.models import Mail

api = Api(app=app)

save_mail_schema = api.namespace('save_emails', description='Main APIs')

post_parser = reqparse.RequestParser()
post_parser.add_argument('body', type=dict, location='json', required=True,)

@save_mail_schema.route("/")
class SaveEmail(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},)
    @save_mail_schema.expect(post_parser)
    def post(self):
        """
            Documentation

            - content-type: "application/json"
            ---
            - Body Request
                - {
                    "event_id": 2,
                    "email_subject": "Haloo Again",
                    "email_content": "Testing Email Haloo Again",
                    "timestamp": "15 Dec 2015 23:12"
                }
        """
        content = request.json

        if not content['event_id']:
            raise BadRequest()
        if not content['email_subject']:
            raise BadRequest()
        if not content['email_content']:
            raise BadRequest()
        if not content['timestamp']:
            raise BadRequest()

        event_id = content['event_id']
        email_subject = content['email_subject']
        email_content = content['email_content']
        timestamp = content['timestamp']
        date_time = datetime.datetime.strptime(timestamp, "%d %b %Y %H:%M")

        mail = Mail(event_id=event_id,
                    email_subject=email_subject,
                    email_content=email_content,
                    timestamp=date_time)
        db.session.add(mail)
        db.session.commit()
        return jsonify({"result": content})

if __name__ == "__main__":
    app.run(host='0.0.0.0')