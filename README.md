Requirements:
- Flask
- gunicorn
- Flask-Mail
- flask-sqlalchemy
- flask-migrate
- celery
- redis
- Flask-Admin
- flask-restplus

##
This Project built with python 3.6, with OS Linux Ubuntu 18.10 LTS

1. Create virtual environment:
    ````
    $ python3 -m venv venv
    ````

2. Then source to virtual environment (venv):
    ````
    $ source venv/bin/activate
    ````

3. Install all libraries in requirements.txt:
    ````
    $ pip install -r requirements.txt
    ````

##
For database, this project using SQLite.

1. Sync database using this command:
    ````
    $ flask db init
    ````

2. Migrate database:
    ````
    $ flask db migrate
    ````

3. Upgrade database:
    ````
    $ flask db upgrade
    ````

##
Configuration:

Please change configuration in the 'config.py' file:
````
CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'

MAIL_SERVER = "smtp.gmail.com"
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = "me@gmail.com"
MAIL_PASSWORD = "******"
MAIL_DEFAULT_SENDER = 'me@gmail.com'
````

##
Make sure redis is already run.

1. Run scheduler:

    ````
    $ celery -A app.celery beat --schedule=/tmp/celerybeat-schedule --loglevel=INFO --pidfile=/tmp/celerybeat.pid
    ````

2. Run celery worker:

    ````
    $ celery -A app.celery worker --loglevel=INFO
    ````

3. Run application:

    - recomended:
    
    ````
    $ gunicorn "wsgi:app"
    ````
    
    - or (not recomended):
    
    ````
    $ flask run
    ````

#
#####Documentation

![Main](tmp/main.png)

#
#####/admin

![Dashboar](tmp/dash.png)

